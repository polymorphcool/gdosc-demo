# gdosc-demo
Godot engine demo project related to https://github.com/djiamnot/gdosc repositiory.

![screenshot](https://frankiezafe.org/images/a/ad/Gdosc-receiver-sender-demo-editor-screenshot.png)
